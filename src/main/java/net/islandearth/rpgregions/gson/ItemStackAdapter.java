package net.islandearth.rpgregions.gson;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Type;
import java.util.Map;

public class ItemStackAdapter implements JsonSerializer<ItemStack>, JsonDeserializer<ItemStack> {

    private final Gson gson;

    public ItemStackAdapter() {
        this.gson = new GsonBuilder().create();
    }

    @Override
    public ItemStack deserialize(JsonElement jsonElement, Type type,
                                 JsonDeserializationContext context) throws JsonParseException {
        return ItemStack.deserialize(gson.fromJson(jsonElement, new TypeToken<Map<String, Object>>(){}.getType()));
    }

    @Override
    public JsonElement serialize(ItemStack itemStack, Type type, JsonSerializationContext context) {
        return gson.toJsonTree(itemStack.serialize());
    }
}