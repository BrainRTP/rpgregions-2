package net.islandearth.rpgregions.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Subcommand;
import com.github.stefvanschie.inventoryframework.Gui;
import com.github.stefvanschie.inventoryframework.GuiItem;
import com.github.stefvanschie.inventoryframework.pane.PaginatedPane;
import com.github.stefvanschie.inventoryframework.pane.StaticPane;
import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.managers.data.region.ConfiguredRegion;
import net.islandearth.rpgregions.rewards.ItemReward;
import net.islandearth.rpgregions.translation.Translations;
import net.islandearth.rpgregions.utils.ItemStackBuilder;
import net.islandearth.rpgregions.utils.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

@CommandAlias("rpgregions")
public class RPGRegionsCommand extends BaseCommand {

	private final RPGRegions plugin;

	public RPGRegionsCommand(RPGRegions plugin) {
		this.plugin = plugin;
	}

	@Default
	public void onDefault(CommandSender sender, String[] args) {
		sender.sendMessage(ChatColor.YELLOW + "Wiki > " + plugin.getDescription().getWebsite());
		sender.sendMessage(ChatColor.YELLOW + "Bugs > https://gitlab.com/SamB440/rpgregions-2");
		sender.sendMessage(ChatColor.YELLOW + "Discord > https://discord.gg/fh62mxU");
		int rewards = 0;
		for (ConfiguredRegion configuredRegion : plugin.getManagers().getRegionsCache().getConfiguredRegions().values()) {
			rewards += configuredRegion.getRewards().size();
		}
		sender.sendMessage(ChatColor.LIGHT_PURPLE + "" +
				plugin.getManagers().getRegionsCache().getConfiguredRegions().size()
				+ " regions are loaded with " + rewards + " rewards.");
	}

	@Subcommand("about")
	public void onAbout(CommandSender sender) {
		sender.sendMessage(StringUtils.colour("&eRPGRegions v" + plugin.getDescription().getVersion() + "."));
		sender.sendMessage(StringUtils.colour("&eOwner: https://www.spigotmc.org/members/%%__USER__%%/"));
	}

	@Subcommand("add")
	@CommandPermission("rpgregions.add")
	public void onAdd(Player player, String[] args) {
		if (args.length == 1) {
			String region = args[0];
			int x = player.getLocation().getBlockX();
			int z = player.getLocation().getBlockZ();
			ConfiguredRegion configuredRegion = new ConfiguredRegion(region, region, new ArrayList<>(), x, z);
			plugin.getManagers().getRegionsCache().addConfiguredRegion(configuredRegion);
			player.sendMessage(StringUtils.colour("&aAdded configured region " + region + "!"));
			player.sendMessage(StringUtils.colour("&e&oNow use /rpgregions edit "
					+ region
					+ " to edit it!"));
		} else {
			player.sendMessage(StringUtils.colour("&cUsage: /rpgregions add <region>"));
		}
	}

	@Subcommand("remove")
	@CommandPermission("rpgregions.remove")
	public void onRemove(CommandSender sender, String[] args) {
		if (args.length == 1) {
			String region = args[0];
			ConfiguredRegion configuredRegion = plugin.getManagers().getRegionsCache().getConfiguredRegion(region);
			if (configuredRegion != null) {
				configuredRegion.delete(plugin);
				plugin.getManagers().getRegionsCache().removeConfiguredRegion(region);
				sender.sendMessage(StringUtils.colour("&cRemoved configured region " + region + "!"));
			} else {
				sender.sendMessage(StringUtils.colour("&cA region by that name is not yet configured."));
			}
		} else {
			sender.sendMessage(StringUtils.colour("&cUsage: /rpgregions remove <region>"));
		}
	}

	@Subcommand("edit")
	@CommandPermission("rpgregions.edit")
	public void onEdit(Player player, String[] args) {
		if (args.length == 1) {//TODO gui
			String region = args[0];
			/*Gui gui = new Gui(plugin, 1, "Editing: " + region);
			OutlinePane pane = new OutlinePane(0, 0, 9, 1);
			GuiItem item = new GuiItem(new ItemStackBuilder(Material.BARRIER)
					.withName(ChatColor.RED + "Close")
					.withLore(StringUtils.colour("&f&oThe edit feature is currently not available."))
					.build(),
					e -> player.closeInventory());
			pane.insertItem(item, 8);
			gui.addPane(pane);
			player.openInventory(gui.getInventory());*/
			player.sendMessage(ChatColor.RED + "Not ready yet! Coming in a future update.");
		} else {
			player.sendMessage(ChatColor.RED + "Usage: /rpgregions edit <region>");
		}
	}

	@Subcommand("list|discoveries")
	@CommandPermission("rpgregions.list")
	public void onList(Player player) {
		Gui gui = new Gui(plugin, 6, Translations.REGIONS.get(player));
		PaginatedPane pane = new PaginatedPane(0, 0, 9, 5);
		StaticPane back = new StaticPane(0, 5, 1, 1);
		StaticPane forward = new StaticPane(8, 5, 1, 1);

		back.addItem(new GuiItem(new ItemStackBuilder(Material.ARROW)
				.withName(Translations.PREVIOUS_PAGE.get(player))
				.build(), event -> {
			event.setCancelled(true);
			if (pane.getPages() == 0) return;

			pane.setPage(pane.getPage() - 1);

			if (pane.getPage() == 0) {
				back.setVisible(false);
			}

			forward.setVisible(true);
			gui.update();
		}), 0, 0);

		back.setVisible(false);

		forward.addItem(new GuiItem(new ItemStackBuilder(Material.ARROW)
				.withName(Translations.NEXT_PAGE.get(player))
				.build(), event -> {
			event.setCancelled(true);
			if (pane.getPages() == 0) return;

			pane.setPage(pane.getPage() + 1);

			if (pane.getPage() == pane.getPages() - 1) {
				forward.setVisible(false);
			}

			back.setVisible(true);
			gui.update();
		}), 0, 0);

		gui.addPane(back);
		gui.addPane(forward);

		plugin.getManagers().getStorageManager().getAccount(player.getUniqueId()).thenAccept(account -> {
			List<GuiItem> items = new ArrayList<>();
			for (ConfiguredRegion configuredRegion : plugin.getManagers().getRegionsCache().getConfiguredRegions().values()) {
				boolean hasDiscovered = account.getDiscoveredRegions().containsKey(configuredRegion.getId());
				if (!hasDiscovered && !player.hasPermission("rpgregions.show")) continue;

				ChatColor colour = hasDiscovered
						? ChatColor.GREEN
						: ChatColor.RED;
				String lore = account.getDiscoveredRegions().containsKey(configuredRegion.getId())
						? Translations.DISCOVERED_ON.get(player,
							account.getDiscoveredRegions().get(configuredRegion.getId()).getDate())
						: "";
				String lore2 = configuredRegion.isShowCoords() && player.hasPermission("rpgregions.showloc")
						? ChatColor.GRAY + "" + configuredRegion.getX() + ", " + configuredRegion.getZ()
						: "";
				items.add(new GuiItem(new ItemStackBuilder(configuredRegion.getIcon())
						.withName(colour + configuredRegion.getCustomName())
						.withLore(lore)
						.withLore(lore2)
						.addFlags(ItemFlag.HIDE_ATTRIBUTES)
						.build(),
						event -> event.setCancelled(true)));
			}

			pane.populateWithGuiItems(items);
			gui.addPane(pane);
			gui.show(player);
		});
	}

	@Subcommand("additem")
	@CommandPermission("rpgregions.additem")
	public void onAddItem(Player player, String[] args) {
		if (args.length > 0) {
			String region = args[0];
			ConfiguredRegion configuredRegion = plugin.getManagers().getRegionsCache().getConfiguredRegion(region);
			if (configuredRegion != null) {
				configuredRegion.getRewards().add(new ItemReward(player.getInventory().getItemInMainHand()));
				player.sendMessage(ChatColor.GREEN + "Item added to configuration!");
			} else {
				player.sendMessage(ChatColor.RED + "No region exists by that name.");
			}
		}
	}

	@Subcommand("reload")
	@CommandPermission("rpgregions.reload")
	public void onReload(CommandSender sender, String[] args) {
		sender.sendMessage(ChatColor.GREEN + "Reloading region files...");
		long startTime = System.currentTimeMillis();
		File folder = new File(plugin.getDataFolder() + "/regions/");
		plugin.getManagers().getRegionsCache().getConfiguredRegions().clear();

		for (File file : folder.listFiles()) {
			// Exclude non-json files
			if (file.getName().endsWith(".json")) {
				try {
					Reader reader = new FileReader(file);
					ConfiguredRegion region = plugin.getGson().fromJson(reader, ConfiguredRegion.class);
					if (!region.getId().equals("exampleconfig")) plugin.getManagers().getRegionsCache().addConfiguredRegion(region);
					reader.close();
				} catch (Exception e) {
					plugin.getLogger().severe("Error loading region config " + file.getName() + ":");
					e.printStackTrace();
				}
			}
		}

		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		sender.sendMessage(ChatColor.GREEN + "Done! (" + totalTime + "ms)");
	}

	@Subcommand("save")
	@CommandPermission("rpgregions.save")
	public void onSave(CommandSender sender, String[] args) {
		sender.sendMessage(ChatColor.GREEN + "Saving data...");
		long startTime = System.currentTimeMillis();

		// Save all player data (quit event not called for shutdown)
		Bukkit.getOnlinePlayers().forEach(player -> {
			if (plugin.getManagers().getStorageManager().getCachedAccounts().containsKey(player.getUniqueId()))
				plugin.getManagers().getStorageManager().removeCachedAccount(player.getUniqueId());
		});

		// Save all region configs
		plugin.getManagers().getRegionsCache().getConfiguredRegions().forEach((id, region) -> {
			try {
				region.save(plugin);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});

		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		sender.sendMessage(ChatColor.GREEN + "Done! (" + totalTime + "ms)");
	}
}
