package net.islandearth.rpgregions.utils;

import net.islandearth.rpgregions.RPGRegions;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

public class TitleAnimator {

	public TitleAnimator(Player player, RPGRegions plugin, List<String> titles, List<String> subtitles, int speed) {
		new BukkitRunnable() {
			int current = 0;
			@Override
			public void run() {
				String title = current < titles.size() ? titles.get(current) : "";
				String subtitle = current < subtitles.size() ? subtitles.get(current) : "";
				if (current > titles.size()
					&& current > subtitles.size()) {
					this.cancel();
					return;
				}

				player.sendTitle(title,
						subtitle,
						plugin.getConfig().getInt("settings.server.discoveries.discovered.title.fadein"),
						plugin.getConfig().getInt("settings.server.discoveries.discovered.title.stay"),
						plugin.getConfig().getInt("settings.server.discoveries.discovered.title.fadeout"));
				current++;
			}
		}.runTaskTimer(plugin, 0L, speed);
	}
}
