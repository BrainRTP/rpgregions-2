package net.islandearth.rpgregions.listener;

import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.api.event.RegionDiscoverEvent;
import net.islandearth.rpgregions.api.event.RegionsEnterEvent;
import net.islandearth.rpgregions.managers.data.region.ConfiguredRegion;
import net.islandearth.rpgregions.managers.data.region.Discovery;
import net.islandearth.rpgregions.managers.data.region.WorldDiscovery;
import net.islandearth.rpgregions.translation.Translations;
import net.islandearth.rpgregions.utils.TitleAnimator;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class RegionListener implements Listener {

	private final RPGRegions plugin;

	public RegionListener(RPGRegions plugin) {
		this.plugin = plugin;
	}

	/**
	 * Handles region discoveries on enter (WG 7).
	 */
	@EventHandler
	public void onEnter(RegionsEnterEvent ree) {
		Player player = ree.getPlayer();
		plugin.getManagers().getStorageManager().getAccount(player.getUniqueId()).thenAccept(account -> {
			for (String region : ree.getRegions()) {
				if (plugin.getManagers().getRegionsCache().getConfiguredRegions().containsKey(region)) {
					boolean has = false;
					for (Discovery discoveredRegion : account.getDiscoveredRegions().values()) {
						if (discoveredRegion.getRegion().equals(region)) {
							has = true;
							break;
						}
					}

					if (!has) {
						LocalDateTime date = LocalDateTime.now();
						DateTimeFormatter format = DateTimeFormatter.ofPattern(plugin.getConfig().getString("settings.server.discoveries.date.format"));

						String formattedDate = date.format(format);
						account.addDiscovery(new WorldDiscovery(formattedDate, region));
						Bukkit.getPluginManager().callEvent(new RegionDiscoverEvent(player, region));
					}
				}
			}
		});
	}

	@EventHandler
	public void onDiscover(RegionDiscoverEvent rde) {
		Player player = rde.getPlayer();
		String region = rde.getRegion();
		if (plugin.getManagers().getRegionsCache().getConfiguredRegions().containsKey(region)) {
			ConfiguredRegion configuredRegion = plugin.getManagers().getRegionsCache().getConfiguredRegion(region);
			List<String> title = Translations.DISCOVERED_TITLE.getList(player, configuredRegion.getCustomName());
			List<String> subtitle = Translations.DISCOVERED_SUBTITLE.getList(player, configuredRegion.getCustomName());
			new TitleAnimator(player,
					plugin,
					title,
					subtitle,
					plugin.getConfig().getInt("settings.server.discoveries.discovered.title.animation_speed"));

			if (configuredRegion.getSound() == null) {
				player.playSound(
						player.getLocation(),
						Sound.valueOf(plugin.getConfig().getString("settings.server.discoveries.discovered.sound.name")),
						1,
						plugin.getConfig().getInt("settings.server.discoveries.discovered.sound.pitch")
				);
			} else {
				player.playSound(
						player.getLocation(),
						configuredRegion.getSound(),
						1,
						plugin.getConfig().getInt("settings.server.discoveries.discovered.sound.pitch")
				);
			}

			configuredRegion.getRewards().forEach(reward -> reward.award(player));
		}
	}
}
