package net.islandearth.rpgregions.listener;

import net.islandearth.rpgregions.RPGRegions;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class MoveListener implements Listener {

	private final RPGRegions plugin;

	public MoveListener(RPGRegions plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onMove(PlayerMoveEvent pme) {
		int x = pme.getTo().getBlockX();
		int z = pme.getTo().getBlockZ();
		int oldX = pme.getFrom().getBlockX();
		int oldZ = pme.getFrom().getBlockZ();
		if (x == oldX && z == oldZ) return;
		
		plugin.getManagers().getIntegrationManager().handleMove(pme);
	}
}