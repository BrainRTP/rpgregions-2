package net.islandearth.rpgregions.listener;

import net.islandearth.rpgregions.RPGRegions;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectionListener implements Listener {

	private final RPGRegions plugin;

	public ConnectionListener(RPGRegions plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent pqe) {
		Player player = pqe.getPlayer();
		if (plugin.getManagers().getStorageManager().getCachedAccounts().containsKey(player.getUniqueId()))
			plugin.getManagers().getStorageManager().removeCachedAccount(player.getUniqueId());
	}
}
