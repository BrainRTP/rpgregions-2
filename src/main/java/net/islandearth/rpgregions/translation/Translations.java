package net.islandearth.rpgregions.translation;

import net.islandearth.languagy.language.Language;
import net.islandearth.rpgregions.RPGRegions;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public enum Translations {
	NEXT_PAGE("&aNext Page"),
	PREVIOUS_PAGE("&cPrevious Page"),
	REGIONS("Regions"),
	DISCOVERED_ON("&7Discovered on: %0"),
	DISCOVERED_TITLE("&d%0 discovered!", true),
	DISCOVERED_SUBTITLE("&fKeep exploring to discover more!", true);

	private final String defaultValue;
	private final boolean isList;
	
	Translations(String defaultValue) {
		this.defaultValue = defaultValue;
		this.isList = false;
	}

	Translations(String defaultValue, boolean isList) {
		this.defaultValue = defaultValue;
		this.isList = isList;
	}
	
	public String getDefaultValue() {
		return defaultValue;
	}

	public boolean isList() {
		return isList;
	}

	private String getPath() {
		return this.toString().toLowerCase();
	}

	public void send(Player player) {
		String message = RPGRegions.getAPI().getTranslator().getTranslationFor(player, this.getPath());
		player.sendMessage(message);
	}

	public void send(Player player, String... values) {
		String message = RPGRegions.getAPI().getTranslator().getTranslationFor(player, this.getPath());
		message = replaceVariables(message, values);
		player.sendMessage(message);
	}

	public void sendList(Player player) {
		List<String> message = RPGRegions.getAPI().getTranslator().getTranslationListFor(player, this.getPath());
		message.forEach(player::sendMessage);
	}

	public void sendList(Player player, String... values) {
		List<String> messages = RPGRegions.getAPI().getTranslator().getTranslationListFor(player, this.getPath());
		messages.forEach(message -> {
			message = replaceVariables(message, values);
			player.sendMessage(message);
		});
	}

	public String get(Player player) {
		return RPGRegions.getAPI().getTranslator().getTranslationFor(player, this.getPath());
	}
	
	public String get(Player player, String... values) {
		String message = RPGRegions.getAPI().getTranslator().getTranslationFor(player, this.getPath());
		message = replaceVariables(message, values);
		return message;
	}

	public List<String> getList(Player player) {
		return RPGRegions.getAPI().getTranslator().getTranslationListFor(player, this.getPath());
	}

	public List<String> getList(Player player, String... values) {
		List<String> messages = new ArrayList<>();
		RPGRegions.getAPI().getTranslator()
				.getTranslationListFor(player, this.getPath())
				.forEach(message -> messages.add(replaceVariables(message, values)));
		return messages;
	}
	
	public static void generateLang(RPGRegions plugin) {
		File lang = new File(plugin.getDataFolder() + "/lang/");
		lang.mkdirs();
		
		for (Language language : Language.values()) {
			File file = new File(plugin.getDataFolder() + "/lang/" + language.getCode() + ".yml");
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}

			FileConfiguration config = YamlConfiguration.loadConfiguration(file);
			config.options().copyDefaults(true);
			for (Translations key : values()) {
				if (!key.isList()) config.addDefault(key.toString().toLowerCase(), key.getDefaultValue());
				else config.addDefault(key.toString().toLowerCase(), Collections.singletonList(key.getDefaultValue()));
			}
			
			try {
				config.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private String replaceVariables(String message, String... values) {
		String modifiedMessage = message;
		for (int i = 0; i < 10; i++) {
			if (values.length > i) modifiedMessage = modifiedMessage.replaceAll("%" + i, values[i]);
			else break;
		}

		return modifiedMessage;
	}
}
