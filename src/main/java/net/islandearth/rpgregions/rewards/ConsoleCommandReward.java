package net.islandearth.rpgregions.rewards;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ConsoleCommandReward extends DiscoveryReward {

	private final String command;

	public ConsoleCommandReward(String command) {
		this.command = command;
	}

	@Override
	public void award(Player player) {
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command.replace("%player%", player.getName()));
	}

	@Override
	public String getName() {
		return "Console Command";
	}
}