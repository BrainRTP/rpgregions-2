package net.islandearth.rpgregions.rewards;

import org.bukkit.entity.Player;

public class PlayerCommandReward extends DiscoveryReward {

	private final String command;

	public PlayerCommandReward(String command) {
		this.command = command;
	}

	@Override
	public void award(Player player) {
		player.performCommand(command.replace("%player%", player.getName()));
	}

	@Override
	public String getName() {
		return "Player Command";
	}
}