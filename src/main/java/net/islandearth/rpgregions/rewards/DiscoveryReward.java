package net.islandearth.rpgregions.rewards;

import org.bukkit.entity.Player;

public abstract class DiscoveryReward {

	/**
	 * Awards this reward to the specified player
	 * @param player
	 */
	public abstract void award(Player player);

	/**
	 * User friendly name of this reward.
	 * @return name of reward
	 */
	public abstract String getName();
}
