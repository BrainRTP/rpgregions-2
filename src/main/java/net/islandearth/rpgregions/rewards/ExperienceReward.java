package net.islandearth.rpgregions.rewards;

import org.bukkit.entity.Player;

public class ExperienceReward extends DiscoveryReward {
	
	private final int xp;
	
	public ExperienceReward(int xp) {
		this.xp = xp;
	}
	
	@Override
	public void award(Player player) {
		player.giveExp(xp);
	}

	@Override
	public String getName() {
		return "Experience";
	}
}
