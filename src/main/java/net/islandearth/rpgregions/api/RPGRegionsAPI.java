package net.islandearth.rpgregions.api;

import net.islandearth.languagy.language.Translator;
import net.islandearth.rpgregions.managers.RPGRegionsManagers;

public interface RPGRegionsAPI {

	/**
	 * Gets the class handling managers.
	 * @return class handling managers
	 */
	RPGRegionsManagers getManagers();

	/**
	 * Gets the translator provided by Languagy
	 * @return Translator
	 */
	Translator getTranslator();
}
