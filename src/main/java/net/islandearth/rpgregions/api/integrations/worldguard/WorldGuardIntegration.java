package net.islandearth.rpgregions.api.integrations.worldguard;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.api.event.RegionsEnterEvent;
import net.islandearth.rpgregions.api.integrations.IntegrationManager;
import net.islandearth.rpgregions.managers.data.region.ConfiguredRegion;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class WorldGuardIntegration implements IntegrationManager {

	private final RPGRegions plugin;
	
	public WorldGuardIntegration(RPGRegions plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean isInRegion(Location location) {
		return WorldGuard.getInstance().getPlatform()
				.getRegionContainer()
				.get(BukkitAdapter.adapt(location.getWorld()))
				.getApplicableRegions(BlockVector3.at(location.getX(), location.getY(), location.getZ())).size() > 0;
	}

	@Override
	public void handleMove(PlayerMoveEvent pme) {
		Player player = pme.getPlayer();
		int x = pme.getTo().getBlockX();
		int y = pme.getTo().getBlockY();
		int z = pme.getTo().getBlockZ();
		Set<ProtectedRegion> regions = this.getProtectedRegions(new Location(player.getWorld(), x, y, z));

		List<String> stringRegions = new ArrayList<>();
		regions.forEach(region -> stringRegions.add(region.getId()));
		Bukkit.getPluginManager().callEvent(new RegionsEnterEvent(player, stringRegions));
	}
	
	@Override
	public ConfiguredRegion getPrioritisedRegion(Location location) {
		Set<ProtectedRegion> regions = WorldGuard.getInstance()
				.getPlatform()
				.getRegionContainer()
				.get(BukkitAdapter.adapt(location.getWorld()))
				.getApplicableRegions(BlockVector3.at(location.getX(), location.getY(), location.getZ()))
				.getRegions();
		ProtectedRegion highest = null;
		for (ProtectedRegion region : regions) {
			if (highest == null) {
				highest = region;
				continue;
			}
			
			if (region.getPriority() >= highest.getPriority()) {
				highest = region;
			}
		}
		
		return plugin.getManagers().getRegionsCache().getConfiguredRegion(highest.getId());
	}
	
	private Set<ProtectedRegion> getProtectedRegions(Location location) {
		return WorldGuard.getInstance()
				.getPlatform()
				.getRegionContainer()
				.get(BukkitAdapter.adapt(location.getWorld()))
				.getApplicableRegions(BlockVector3.at(location.getX(), location.getY(), location.getZ()))
				.getRegions();
	}
}
