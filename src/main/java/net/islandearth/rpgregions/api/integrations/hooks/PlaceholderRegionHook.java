package net.islandearth.rpgregions.api.integrations.hooks;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import net.islandearth.rpgregions.RPGRegions;
import org.bukkit.entity.Player;

public class PlaceholderRegionHook extends PlaceholderExpansion {
	
	private final RPGRegions plugin;
	
	public PlaceholderRegionHook(RPGRegions plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean persist() {
		return true;
	}
	
	@Override
	public boolean canRegister() {
		return true;
	}
	
	@Override
	public String getAuthor() {
		return plugin.getDescription().getAuthors().toString();
	}
	
	@Override
	public String getIdentifier() {
		return "rpgregions";
	}
	
	@Override
	public String getVersion() {
		return plugin.getDescription().getVersion();
	}
	
	@Override
	public String onPlaceholderRequest(Player player, String identifier) {
		if (player == null) return "";
		
		if (identifier.equals("region")) {
			return plugin.getManagers().getIntegrationManager().getPrioritisedRegion(player.getLocation()).getId();
		}
		
		return null;
	}
}
