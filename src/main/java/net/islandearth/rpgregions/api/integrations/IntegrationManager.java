package net.islandearth.rpgregions.api.integrations;

import net.islandearth.rpgregions.managers.data.region.ConfiguredRegion;
import org.bukkit.Location;
import org.bukkit.event.player.PlayerMoveEvent;

public interface IntegrationManager {

	/**
	 * Checks if the specified location is within a region.
	 * @param location location to check
	 * @return true if location is within a region, false otherwise
	 */
	boolean isInRegion(Location location);

	/**
	 * Handles a move event to perform related region checks.
	 * @param pme PlayerMoveEvent
	 */
	void handleMove(PlayerMoveEvent pme);
	
	/**
	 * Gets the highest priority region at given location
	 * @param location location to check
	 * @return highest prioritised region
	 */
	ConfiguredRegion getPrioritisedRegion(Location location);

}
