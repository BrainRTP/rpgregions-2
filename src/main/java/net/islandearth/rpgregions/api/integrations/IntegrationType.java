package net.islandearth.rpgregions.api.integrations;

import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.api.integrations.worldguard.WorldGuardIntegration;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

public enum IntegrationType {
	WORLDGUARD(WorldGuardIntegration.class);

	private final Class<? extends IntegrationManager> clazz;

	IntegrationType(Class<? extends IntegrationManager> clazz) {
		this.clazz = clazz;
	}

	public Optional<IntegrationManager> get() {
		RPGRegions plugin = JavaPlugin.getPlugin(RPGRegions.class);
		plugin.getLogger().info("Loading IntegrationManager implementation...");
		IntegrationManager generatedClazz = null;
		try {
			generatedClazz = clazz.getConstructor(RPGRegions.class).newInstance(JavaPlugin.getPlugin(RPGRegions.class));
			plugin.getLogger().info("Loaded IntegrationManager implementation " + clazz.getName() + ".");
		} catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
			plugin.getLogger().severe("Unable to load StorageManager (" + clazz.getName() + ")! Plugin will disable.");
			e.printStackTrace();
			Bukkit.getPluginManager().disablePlugin(plugin);
		}

		return Optional.ofNullable(generatedClazz);
	}
}
