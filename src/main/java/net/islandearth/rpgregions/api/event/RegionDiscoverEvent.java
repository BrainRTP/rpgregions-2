package net.islandearth.rpgregions.api.event;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

import java.util.Set;

public class RegionDiscoverEvent extends Event {

	private static final HandlerList HANDLER_LIST = new HandlerList();
	private final Player player;
	private final String region;
	
	public RegionDiscoverEvent(Player player, String region) {
		this.player = player;
		this.region = region;
	}
	
	@NotNull
	public Player getPlayer() {
		return player;
	}

	@NotNull
	public String getRegion() {
		return region;
	}

	@NotNull
	@Override
	public HandlerList getHandlers() {
		return HANDLER_LIST;
	}

	public static HandlerList getHandlerList() {
		return HANDLER_LIST;
	}

}