package net.islandearth.rpgregions.api.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class RegionsEnterEvent extends Event {

	private static final HandlerList HANDLER_LIST = new HandlerList();
	private final Player player;
	private final List<String> regions;

	public RegionsEnterEvent(Player player, List<String> regions) {
		this.player = player;
		this.regions = regions;
	}

	@NotNull
	public Player getPlayer() {
		return player;
	}

	@NotNull
	public List<String> getRegions() {
		return regions;
	}

	@NotNull
	@Override
	public HandlerList getHandlers() {
		return HANDLER_LIST;
	}

	public static HandlerList getHandlerList() {
		return HANDLER_LIST;
	}
}