package net.islandearth.rpgregions;

import co.aikar.commands.PaperCommandManager;
import co.aikar.idb.DB;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.islandearth.languagy.language.LanguagyImplementation;
import net.islandearth.languagy.language.LanguagyPluginHook;
import net.islandearth.languagy.language.Translator;
import net.islandearth.rpgregions.api.RPGRegionsAPI;
import net.islandearth.rpgregions.commands.RPGRegionsCommand;
import net.islandearth.rpgregions.gson.AbstractAdapter;
import net.islandearth.rpgregions.gson.ItemStackAdapter;
import net.islandearth.rpgregions.listener.ConnectionListener;
import net.islandearth.rpgregions.listener.MoveListener;
import net.islandearth.rpgregions.listener.RegionListener;
import net.islandearth.rpgregions.managers.RPGRegionsManagers;
import net.islandearth.rpgregions.rewards.DiscoveryReward;
import net.islandearth.rpgregions.translation.Translations;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;

public final class RPGRegions extends JavaPlugin implements RPGRegionsAPI, LanguagyPluginHook {
	
	private RPGRegionsManagers managers;
	private static RPGRegions plugin;

	public Translator getTranslator() {
		return translator;
	}

	@LanguagyImplementation(fallbackFile = "plugins/RPGRegions/lang/en_gb.yml")
	private Translator translator;

	@Override
	public void onEnable() {
		new Metrics(this);
		plugin = this;
		this.createConfig();
		this.generateLang();
		this.managers = new RPGRegionsManagers(this);
		this.registerListeners();
		this.registerCommands();
	}

	@Override
	public void onDisable() {
		// Save all player data (quit event not called for shutdown)
		Bukkit.getOnlinePlayers().forEach(player -> {
			if (managers.getStorageManager().getCachedAccounts().containsKey(player.getUniqueId()))
				this.getManagers().getStorageManager().removeCachedAccount(player.getUniqueId());
		});

		// Save all region configs
		managers.getRegionsCache().getConfiguredRegions().forEach((id, region) -> {
			try {
				region.save(this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		
		// Close database connection
		DB.close();
	}

	@Override
	public RPGRegionsManagers getManagers() {
		return managers;
	}

	private void generateLang() {
		Translations.generateLang(this);
	}

	private void createConfig() {
		FileConfiguration config = this.getConfig();
		String header;
		String eol = System.getProperty("line.separator");
		header = "This is the config for RPGRegions." + eol;
		header += "------ Useful information ------" + eol;
		header += "Documentation can be found at https://fortitude.islandearth.net" + eol;
		header += "Sounds can be found at https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Sound.html" + eol;
		header += "------ Support ------" + eol;
		header += "Found a bug? Create an issue at https://gitlab.com/SamB440/rpgregions-2/issues" + eol;
		header += "Need help? Join our discord at https://discord.gg/fh62mxU" + eol;
		config.options().header(header);
		config.addDefault("settings.integration.name", "WorldGuard");
		config.addDefault("settings.storage.mode", "file");
		config.addDefault("settings.sql.host", "localhost");
		config.addDefault("settings.sql.port", 3306);
		config.addDefault("settings.sql.db", "RPGRegions");
		config.addDefault("settings.sql.user", "user");
		config.addDefault("settings.sql.pass", "pass");
		config.addDefault("settings.server.discoveries.date.format", "dd-MM-yyyy HH:mm:ss");
		config.addDefault("settings.server.discoveries.discovered.title.fadein", 20);
		config.addDefault("settings.server.discoveries.discovered.title.stay", 60);
		config.addDefault("settings.server.discoveries.discovered.title.fadeout", 20);
		config.addDefault("settings.server.discoveries.discovered.sound.name", Sound.UI_TOAST_CHALLENGE_COMPLETE.toString());
		config.addDefault("settings.server.discoveries.discovered.sound.pitch", 1);
		config.addDefault("settings.server.discoveries.discovered.title.animation_speed", 20);
		config.options().copyDefaults(true);
		saveConfig();
	}

	private void registerListeners() {
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new ConnectionListener(this), this);
		pm.registerEvents(new RegionListener(this), this);
		pm.registerEvents(new MoveListener(this), this);
	}

	private void registerCommands() {
		PaperCommandManager manager = new PaperCommandManager(this);
		manager.registerCommand(new RPGRegionsCommand(this));
	}

	public static RPGRegionsAPI getAPI() {
		return plugin;
	}

	@Override
	public void onLanguagyHook() {
		translator.setDisplay(Material.MAP);
	}

	public Gson getGson() {
		return new GsonBuilder()
				.registerTypeAdapter(DiscoveryReward.class, new AbstractAdapter<DiscoveryReward>(null))
				.registerTypeHierarchyAdapter(ItemStack.class, new ItemStackAdapter())
				.setPrettyPrinting()
				.serializeNulls().create();
	}
}
