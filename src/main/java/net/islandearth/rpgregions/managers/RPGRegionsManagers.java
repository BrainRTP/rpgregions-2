package net.islandearth.rpgregions.managers;

import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.api.integrations.IntegrationManager;
import net.islandearth.rpgregions.api.integrations.IntegrationType;
import net.islandearth.rpgregions.api.integrations.hooks.PlaceholderRegionHook;
import net.islandearth.rpgregions.managers.data.RPGRegionsCache;
import net.islandearth.rpgregions.managers.data.StorageManager;
import net.islandearth.rpgregions.managers.data.StorageType;
import net.islandearth.rpgregions.managers.data.region.ConfiguredRegion;
import net.islandearth.rpgregions.rewards.*;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class RPGRegionsManagers {

	private StorageManager storageManager;
	private IntegrationManager integrationManager;
	private RPGRegionsCache regionsCache;

	public RPGRegionsManagers(RPGRegions plugin) {
		StorageType.valueOf(plugin.getConfig().getString("settings.storage.mode").toUpperCase())
				.get()
				.ifPresent(storageManager1 -> storageManager = storageManager1);
		if (storageManager == null) throw new IllegalStateException("Could not find StorageManager!");

		IntegrationType.valueOf(plugin.getConfig().getString("settings.integration.name").toUpperCase())
				.get()
				.ifPresent(integrationManager1 -> integrationManager = integrationManager1);
		if (integrationManager == null) throw new IllegalStateException("Could not find IntegrationManager!");
		
		this.regionsCache = new RPGRegionsCache(plugin);

		File folder = new File(plugin.getDataFolder() + "/regions/");
		if (!folder.exists()) folder.mkdirs();

		// Generate an example config
		List<DiscoveryReward> rewards = new ArrayList<>();
		rewards.add(new ExperienceReward(10));
		rewards.add(new ItemReward(new ItemStack(Material.IRON_BARS)));
		rewards.add(new PlayerCommandReward("say I discovered a region!"));
		rewards.add(new ConsoleCommandReward("say Server sees you discovered a region!"));
		ConfiguredRegion configuredRegion = new ConfiguredRegion("exampleconfig", "ExampleConfig", rewards,
				Sound.AMBIENT_UNDERWATER_EXIT,
				Material.WOODEN_AXE,
				0,
				0);
		try {
			configuredRegion.save(plugin);
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (File file : folder.listFiles()) {
			// Exclude non-json files
			if (file.getName().endsWith(".json")) {
				try {
					Reader reader = new FileReader(file);
					ConfiguredRegion region = plugin.getGson().fromJson(reader, ConfiguredRegion.class);
					if (!region.getId().equals("exampleconfig")) regionsCache.addConfiguredRegion(region);
					reader.close();
				} catch (Exception e) {
					plugin.getLogger().severe("Error loading region config " + file.getName() + ":");
					e.printStackTrace();
				}
			}
		}
		
		if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
			new PlaceholderRegionHook(plugin).register();
		}
	}

	public StorageManager getStorageManager() {
		return storageManager;
	}

	public IntegrationManager getIntegrationManager() {
		return integrationManager;
	}
	
	public RPGRegionsCache getRegionsCache() {
		return regionsCache;
	}
}
