package net.islandearth.rpgregions.managers.data.account;

import net.islandearth.rpgregions.managers.data.region.Discovery;

import java.util.Map;
import java.util.UUID;

public class RPGRegionsAccount {

	private UUID uuid;
	private Map<String, Discovery> discoveredRegions;

	public RPGRegionsAccount(UUID uuid, Map<String, Discovery> discoveredRegions) {
		this.uuid = uuid;
		this.discoveredRegions = discoveredRegions;
	}

	public Map<String, Discovery> getDiscoveredRegions() {
		return discoveredRegions;
	}

	public void addDiscovery(Discovery discovery) {
		discoveredRegions.put(discovery.getRegion(), discovery);
	}
}