package net.islandearth.rpgregions.managers.data;

import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.managers.data.sql.SqlStorage;
import net.islandearth.rpgregions.managers.data.yml.YamlStorage;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

public enum StorageType {
	FILE(YamlStorage.class),
	SQL(SqlStorage.class);

	private final Class<? extends StorageManager> clazz;

	StorageType(Class<? extends StorageManager> clazz) {
		this.clazz = clazz;
	}

	public Optional<StorageManager> get() {
		RPGRegions plugin = JavaPlugin.getPlugin(RPGRegions.class);
		plugin.getLogger().info("Loading StorageManager implementation...");
		StorageManager generatedClazz = null;
		try {
			generatedClazz = clazz.getConstructor(RPGRegions.class).newInstance(JavaPlugin.getPlugin(RPGRegions.class));
			plugin.getLogger().info("Loaded StorageManager implementation " + clazz.getName() + ".");
		} catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
			plugin.getLogger().severe("Unable to load StorageManager (" + clazz.getName() + ")! Plugin will disable.");
			e.printStackTrace();
			Bukkit.getPluginManager().disablePlugin(plugin);
		}

		return Optional.ofNullable(generatedClazz);
	}
}
