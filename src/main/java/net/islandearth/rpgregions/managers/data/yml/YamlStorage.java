package net.islandearth.rpgregions.managers.data.yml;

import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.managers.data.StorageManager;
import net.islandearth.rpgregions.managers.data.account.RPGRegionsAccount;
import net.islandearth.rpgregions.managers.data.region.Discovery;
import net.islandearth.rpgregions.managers.data.region.WorldDiscovery;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class YamlStorage implements StorageManager {

	private ConcurrentMap<UUID, RPGRegionsAccount> cachedAccounts = new ConcurrentHashMap<>();

	private final RPGRegions plugin;

	public YamlStorage(RPGRegions plugin) {
		this.plugin = plugin;
		File dataFile = new File(plugin.getDataFolder() + "/accounts/");
		dataFile.mkdirs();
	}

	@Override
	public CompletableFuture<RPGRegionsAccount> getAccount(UUID uuid) {
		CompletableFuture<RPGRegionsAccount> future = new CompletableFuture<>();
		if (cachedAccounts.containsKey(uuid)) {
			future.complete(cachedAccounts.get(uuid));
		} else {
			File file = new File(plugin.getDataFolder() + "/accounts/" + uuid.toString() + ".yml");
			FileConfiguration config = YamlConfiguration.loadConfiguration(file);
			Map<String, Discovery> regions = new HashMap<>();
			for (String results : config.getStringList("Discoveries")) {
				String[] data = results.split(";");
				String time = data[0];
				String region = data[1];
				regions.put(region, new WorldDiscovery(time, region));
			}

			RPGRegionsAccount account = new RPGRegionsAccount(uuid, regions);
			cachedAccounts.put(uuid, account);
			future.complete(account);
		}
		return future;
	}

	@Override
	public ConcurrentMap<UUID, RPGRegionsAccount> getCachedAccounts() {
		return cachedAccounts;
	}

	@Override
	public void removeCachedAccount(UUID uuid) {
		RPGRegionsAccount account = cachedAccounts.get(uuid);
		File file = new File(plugin.getDataFolder() + "/accounts/" + uuid.toString() + ".yml");
		FileConfiguration config = YamlConfiguration.loadConfiguration(file);
		List<Discovery> current = new ArrayList<>();
		for (String results : config.getStringList("Discoveries")) {
			String[] data = results.split(";");
			String time = data[0];
			String region = data[1];
			current.add(new WorldDiscovery(time, region));
		}

		List<String> newData = config.getStringList("Discoveries");
		for (Discovery region : account.getDiscoveredRegions().values()) {
			if (!current.contains(region)) {
				newData.add(region.getDate() + ";" + region.getRegion());
			}
		}

		config.set("Discoveries", newData);
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		cachedAccounts.remove(uuid);
	}
}
