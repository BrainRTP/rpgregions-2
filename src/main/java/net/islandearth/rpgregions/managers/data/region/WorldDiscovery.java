package net.islandearth.rpgregions.managers.data.region;

public class WorldDiscovery implements Discovery {

	private String date;
	private String region;

	public WorldDiscovery(String date, String region) {
		this.date = date;
		this.region = region;
	}

	@Override
	public String getDate() {
		return date;
	}

	@Override
	public String getRegion() {
		return region;
	}
}
