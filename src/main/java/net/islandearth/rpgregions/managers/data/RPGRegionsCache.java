package net.islandearth.rpgregions.managers.data;

import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.managers.data.region.ConfiguredRegion;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class RPGRegionsCache {

	private final RPGRegions plugin;
	private Map<String, ConfiguredRegion> configuredRegions = new ConcurrentHashMap<>();

	public RPGRegionsCache(RPGRegions plugin) {
		this.plugin = plugin;
	}

	@Nullable
	public ConfiguredRegion getConfiguredRegion(String id) {
		return configuredRegions.get(id);
	}

	public void addConfiguredRegion(ConfiguredRegion region) {
		configuredRegions.put(region.getId(), region);
	}

	public void removeConfiguredRegion(String id) {
		configuredRegions.remove(id);
	}

	public Map<String, ConfiguredRegion> getConfiguredRegions() {
		return configuredRegions;
	}
}
