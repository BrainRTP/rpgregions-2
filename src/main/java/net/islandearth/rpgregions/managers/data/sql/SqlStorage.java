package net.islandearth.rpgregions.managers.data.sql;

import co.aikar.idb.*;
import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.managers.data.StorageManager;
import net.islandearth.rpgregions.managers.data.account.RPGRegionsAccount;
import net.islandearth.rpgregions.managers.data.region.Discovery;
import net.islandearth.rpgregions.managers.data.region.WorldDiscovery;

import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class SqlStorage implements StorageManager {
	
	private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS rpgregions_discoveries (uuid varchar(32) NOT NULL, region varchar(32) NOT NULL, time varchar(64) NOT NULL, PRIMARY KEY(uuid, region))";
	private static final String SELECT_REGION = "SELECT * FROM rpgregions_discoveries WHERE uuid = ?";
	private static final String INSERT_DISCOVERY = "INSERT INTO rpgregions_discoveries (uuid, region, time) VALUES (?, ?, ?)";

	private ConcurrentMap<UUID, RPGRegionsAccount> cachedAccounts = new ConcurrentHashMap<>();

	public SqlStorage(RPGRegions plugin) {
		DatabaseOptions options = DatabaseOptions.builder().mysql(plugin.getConfig().getString("settings.sql.user"),
				plugin.getConfig().getString("settings.sql.pass"),
				plugin.getConfig().getString("settings.sql.db"),
				plugin.getConfig().getString("settings.sql.host") + ":" + plugin.getConfig().getString("settings.sql.port")).build();
		Database db = PooledDatabaseOptions.builder().options(options).createHikariDatabase();
		DB.setGlobalDatabase(db);
		try {
			db.executeUpdate(CREATE_TABLE);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public CompletableFuture<RPGRegionsAccount> getAccount(UUID uuid) {
		CompletableFuture<RPGRegionsAccount> future = new CompletableFuture<>();
		if (cachedAccounts.containsKey(uuid)) {
			future.complete(cachedAccounts.get(uuid));
		} else {
			DB.getResultsAsync(SELECT_REGION, getDatabaseUuid(uuid)).thenAccept(results -> {
				Map<String, Discovery> regions = new HashMap<>();
				for (DbRow row : results) {
					String region = row.getString("region");
					regions.put(region, new WorldDiscovery(row.getString("time"), region));
				}

				RPGRegionsAccount account = new RPGRegionsAccount(uuid, regions);
				cachedAccounts.put(uuid, account);
				future.complete(account);
			});
		}
		return future;
	}

	@Override
	public ConcurrentMap<UUID, RPGRegionsAccount> getCachedAccounts() {
		return cachedAccounts;
	}

	@Override
	public void removeCachedAccount(UUID uuid) {
		RPGRegionsAccount account = cachedAccounts.get(uuid);
		DB.getResultsAsync(SELECT_REGION, getDatabaseUuid(uuid)).thenAccept(results -> {
			List<String> current = new ArrayList<>();
			for (DbRow row : results) {
				current.add(row.getString("region"));
			}

			for (Discovery region : account.getDiscoveredRegions().values()) {
				if (!current.contains(region.getRegion())) {
					try {
						DB.executeInsert(INSERT_DISCOVERY, getDatabaseUuid(uuid), region.getRegion(), region.getDate());
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			cachedAccounts.remove(uuid);
		});
	}
}
