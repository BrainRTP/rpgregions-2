package net.islandearth.rpgregions.managers.data.region;

import com.google.gson.Gson;
import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.rewards.DiscoveryReward;
import org.bukkit.Material;
import org.bukkit.Sound;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

public class ConfiguredRegion {

	private final String id;
	private final String customName;
	private final List<DiscoveryReward> rewards;
	private final Sound sound;
	private final Material icon;
	private final boolean showCoords;
	private final int x;
	private final int z;

	public ConfiguredRegion(String id, String customName, List<DiscoveryReward> rewards, int x, int z) {
		this.id = id;
		this.customName = customName;
		this.rewards = rewards;
		this.sound = Sound.UI_TOAST_CHALLENGE_COMPLETE;
		this.icon = Material.TOTEM_OF_UNDYING;
		this.showCoords = false;
		this.x = x;
		this.z = z;
	}

	public ConfiguredRegion(String id, String customName,
							List<DiscoveryReward> rewards, Sound sound, Material icon, int x, int z) {
		this.id = id;
		this.customName = customName;
		this.rewards = rewards;
		this.sound = sound;
		this.icon = icon;
		this.showCoords = false;
		this.x = x;
		this.z = z;
	}

	public String getId() {
		return id;
	}

	public String getCustomName() {
		return customName;
	}

	public List<DiscoveryReward> getRewards() {
		return rewards;
	}

	public Sound getSound() {
		return sound;
	}

	public Material getIcon() {
		return icon;
	}

	public int getX() {
		return x;
	}

	public int getZ() {
		return z;
	}

	public boolean isShowCoords() {
		return showCoords;
	}

	public void save(RPGRegions plugin) throws IOException {
		File file = new File(plugin.getDataFolder() + "/regions/" + this.id + ".json");
		Writer writer = new FileWriter(file);
		Gson gson = plugin.getGson();
		gson.toJson(this, writer);
		writer.flush();
		writer.close();
	}

	public void delete(RPGRegions plugin) {
		File file = new File(plugin.getDataFolder() + "/regions/" + this.id + ".json");
		file.delete();
	}
}
