![rpgregions](https://fortitude.islandearth.net/rpgregions.png)
# RPGRegions 2
RPGRegions 2 is the successor to RPGRegions 1. The entire plugin has been recoded and has an entirely new design. With performance in mind, it is the only discovery plugin that is up-to-date and supporting SQL.

# Links
- [Spigot](https://spigotmc.org)
- [Wiki](https://fortitude.islandearth.net)
- [Discord](https://discord.gg/fh62mxU)
